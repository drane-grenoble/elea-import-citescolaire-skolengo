# Elea-import-citescolaire-skolengo

## Récupérer le fichier d'import
Dans le cas des cités scolaires, le fichier d'import Skolengo contient les utilisateurs des deux établissements mélangés.
Récupérer le fichier d'import, puis aller sur cette page :
https://drane-grenoble.forge.apps.education.fr/elea-import-citescolaire-skolengo/

Toutes les données resteront dans votre navigateur, aucune donnée n'est envoyée par ce site.

## séparer les utilisateurs
En retour seront télécharger sur votre navigateur autant de fichiers que d'établissements dans la cité scolaire.

