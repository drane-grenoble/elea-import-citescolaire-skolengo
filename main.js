function processCSV(file, classeColumnName) {
    const reader = new FileReader();

    reader.onload = function(event) {
        const csvData = event.target.result;
        const lines = csvData.split('\n');
        const headers = lines[0].split(';');  
 // Récupérer les en-têtes
        const classeIndex = headers.indexOf(classeColumnName);

        if (classeIndex === -1) {
            console.error(`La colonne "${classeColumnName}" n'a pas été trouvée dans le fichier CSV.`);
            return;
        }

        const groupedData = {};

        for (let i = 1; i < lines.length; i++) {
            //Les classes sont séprarées par des virgules, pourquoi les splitter ?
            console.log(lines);
            const row = lines[i].split(';');
            if (row != ''){
                const prefix = row[classeIndex].split('~')[0];
                if (prefix) {
                    groupedData[prefix] = groupedData[prefix] || [];
                    groupedData[prefix].push(row);
                }
            }
        }
        
        const nbEtab=Object.keys(groupedData).length;
        const bilan= document.createElement('p');
        bilan.innerHTML=nbEtab+' établissements ont été trouvés. Pour chaque établissement, un fichier d\'export a été téléchargé sur votre machine. Vous ne devez pas l\'éditer. Pensez à le renommer en export.csv avant de l\'intégrer dans Éléa';
        document.body.appendChild(bilan);

        // Créer les fichiers CSV
        for (const prefix in groupedData) {
            groupedData[prefix].unshift(headers);
        
            // TODO refusionner classes avant avec spérateur 
            const csvContent = groupedData[prefix].map(row => row.join(';')).join('\n');
            const filename = `${prefix}-import.csv`;
            const blob = new Blob([csvContent], { type: 'text/csv;charset=utf-8;' });
            const link = document.createElement('a');
            link.href = URL.createObjectURL(blob);
            link.download  
 = filename;
            link.click();
        }
    };

    reader.readAsText(file);  
}

// Fonction pour gérer l'événement "change" de l'input de fichier
function handleFileSelect(event) {
    const file = event.target.files[0];
    const classeColumnName = 'classe';
    if (file) {
        processCSV(file, classeColumnName);
    } else {
        console.error("Aucun fichier sélectionné");
    }
}

// Créer un élément d'entrée de type fichier
const input = document.createElement('input');
input.type = 'file';
input.accept = '.csv';

// Ajouter un écouteur d'événement pour gérer le changement de fichier
input.addEventListener('change', handleFileSelect);

// Ajouter l'élément input à la page (vous pouvez le placer dans un élément existant)
document.body.appendChild(input);